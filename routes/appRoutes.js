var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/welcomeCtrl');
    router.get('/home',welcomeCtrl.showHomePage);
  var userCtrl = require('./../controllers/userCtrl');
   router.get('/customer/:id', userCtrl.showCustomerDetailPage);
   router.get('/customer',userCtrl.showAllCustomers);
   router.get('/vendor/:u_id',userCtrl.showVendorDetailPage);
   router.get('/vendor',userCtrl.showVendorsPage);
 
   router.post('/login',userCtrl.login);

   router.post('/changepassword',userCtrl.changepassword);


    router.post('/editprofile',userCtrl.editprofile);
    router.get('/home/editprofile',userCtrl.showeditprofilepage);

   
    router.get('/home/signupasvendor',userCtrl.showsignupasVendorPage);
  router.post('/signupasvendor',userCtrl.signup);
  router.post('/signupascustomer',userCtrl.signupascustomer );

  router.get('/admin',userCtrl.showAdminPage);
  
  router.get('/logout',userCtrl.logout);
  
 

   var searchCtrl = require('./../controllers/searchCtrl');
   router.get('/search', searchCtrl.showSearchPage);
   var productCtrl = require('./../controllers/productCtrl');
   router.post('/disableproduct',productCtrl.disableProduct);
   router.get('/product',productCtrl.showAllProducts);
   router.get('/product/:id',productCtrl.showProductDetailPage);
   router.get('/:slug/addproduct',productCtrl.addproductPage);
   router.post('/addproduct',productCtrl.showaddproductPage);
   router.get('/product/addreview/:id',productCtrl.showAddReviewPage);
  router.post('/product/addingreview/:id',productCtrl.showReviewPage);

  var emailCtrl = require('./../controllers/emailCtrl');
  router.get('/email', emailCtrl.sendmail);
  router.get('/mobile', emailCtrl.sendotp);
    return router.middleware();
}
