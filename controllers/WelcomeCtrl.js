var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
   showHomePage: function* (next) {


        /*var userId= this.request.query.id;
        var queryString='select * from user where u_id ="%s"';
        var query=util.format(queryString,userId);
        var result=yield databaseUtils.executeQuery(query);*/


        var queryString_most_review ='SELECT p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" FROM product AS p LEFT JOIN review AS r ON p.p_id = r.p_id where r.review is not null GROUP BY p.p_id ORDER BY count(*) DESC LIMIT 5';
        var result1 =yield databaseUtils.executeQuery(queryString_most_review);
        for(var i in result1){
            var productId = result1[i].p_id;
          var  queryproduct = 'select (select count(*) from review where p_id = "%s" and rating = 5)as r5,(select count(*) from review where p_id = "%s" and rating = 4)AS r4,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 3)as r3,(select count(*) from review where p_id = "%s" and rating = 2)AS r2,(select count(*) from review where p_id = "%s" and rating = 1) as r1';
            
            var query = util.format(queryproduct,productId,productId,productId,productId,productId);
            var result= yield databaseUtils.executeQuery(query);
                 var avg= ((result[0].r5)*25 + (result[0].r4)*16 +   (result[0].r3)*9    +   (result[0].r2)*4    +   (result[0].r1)*1 )/((result[0].r5)*5 + (result[0].r4)*4 +   (result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1);
    
                 result1[i].rating=avg;
        }

        var queryString_top_rated ='SELECT p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" FROM product AS p LEFT JOIN review AS r ON p.p_id = r.p_id GROUP BY p.p_id ORDER BY AVG(r.rating) DESC LIMIT 5';
        var result2=yield databaseUtils.executeQuery(queryString_top_rated);
        for(var i in result2){
            var productId = result2[i].p_id;
          var  queryproduct = 'select (select count(*) from review where p_id = "%s" and rating = 5)as r5,(select count(*) from review where p_id = "%s" and rating = 4)AS r4,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 3)as r3,(select count(*) from review where p_id = "%s" and rating = 2)AS r2,(select count(*) from review where p_id = "%s" and rating = 1) as r1';
            
            var query = util.format(queryproduct,productId,productId,productId,productId,productId);
            var result= yield databaseUtils.executeQuery(query);
                 var avg= ((result[0].r5)*25 + (result[0].r4)*16 +   (result[0].r3)*9    +   (result[0].r2)*4    +   (result[0].r1)*1 )/((result[0].r5)*5 + (result[0].r4)*4 +   (result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1);
    
                 result1[i].rating=avg;
        }


        var queryString_top_distributers='SELECT v.firm_name,v.logo, v.location, COUNT(*) as "added_product" FROM vendor AS v LEFT JOIN vendor_product AS vp ON v.u_id = vp.u_id GROUP BY v.u_id ORDER BY count(*) DESC LIMIT 3';
        var result3=yield databaseUtils.executeQuery(queryString_top_distributers);
        /*var queryString='select * from user where name ="%s"';
        var query=util.format(queryString,'kumud');
        var result=yield databaseUtils.executeQuery(query);*/

        

        yield this.render('home',{
            showDetails:false,
            userDetails1:result1,
            userDetails2:result2,
            userDetails3:result3
        });
    },
    searchPage: function* (next) {
        var pnam = this.request.query.pname;
        var vnam = this.request.query.vname;

        
        
        if(pnam)
        {
        var queryproduct = 'select product.p_id,name,image_url,avg(rating) as rating from product left join review on product.p_id=review.p_id where name like "%s" and product.available=1 group by name;';
        var query=util.format(queryproduct,"%"+pnam+"%");
        var ven = false;
        }
        else{
            var queryvendor = 'select * from vendor where firm_name like "%s"';
            var query=util.format(queryvendor,"%"+vnam+"%");
            var ven=true;
            console.log(query);
        }
        console.log(query);
        var result= yield databaseUtils.executeQuery(query);
        var proDetails=result;
        yield this.render('search',{ 
                product: proDetails,
                isvendor: ven
        });
    },
 
    showcustomerdetailPage: function* (next) {

        userId = this.params.id;
        var queryString_details ='select * from user where u_id ="%s"';
        var query=util.format(queryString_details,userId);

        var queryString_reviewed_product='SELECT p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" ,r.creation_timestamp FROM product AS p JOIN review AS r ON p.p_id = r.p_id WHERE r.p_id IN (SELECT r.p_id FROM review GROUP BY r.u_id HAVING r.u_id = 101 ORDER BY creation_timestamp) GROUP BY p.p_id ORDER BY AVG(r.rating)';
        var result1=yield databaseUtils.executeQuery(queryString_reviewed_product);
    
        var result2=yield databaseUtils.executeQuery(query);
        yield this.render('customerdetailpage',{
            userDetails1:result1,
            userDetails2:result2[0]
        });
    },
    customerPage: function* (next){
                var querycustomer = 'select * from user join user_role on user.u_id=user_role.u_id where user_role.r_id=1;'
                var result = yield databaseUtils.executeQuery(querycustomer);
                yield this.render('customer',{
                    customer: result
                })
    },
    vendorPage: function* (next){
        var queryvendor = 'select * from vendor;'
        var result = yield databaseUtils.executeQuery(queryvendor);
        yield this.render('vendor',{
            vendor: result
        })
    },
    vendordetailPage: function* (next){
        userId= this.params.u_id;
var queryString_Details = 'select * from vendor join user on vendor.u_id=user.u_id where vendor.u_id="%s";';
var query1 = util.format(queryString_Details, userId);
var result4 = yield databaseUtils.executeQuery(query1);
console.log(result4[0]);

var queryString_allproducts = 'select name,image_url,vendor_product.p_id,avg(rating) as avg,product.available, vendor_product.creation_timestamp from ((product join vendor_product on vendor_product.p_id=product.p_id) left join review on review.p_id=vendor_product.p_id) where vendor_product.u_id="%s" group by product.p_id;'
var query2 = util.format(queryString_allproducts, userId);
var result5 = yield databaseUtils.executeQuery(query2);
var queryString_recent = 'SELECT name,image_url,creation_timestamp FROM product JOIN vendor_product ON product.p_id=vendor_product.p_id where u_id="%s" order by creation_timestamp limit 5;';
var query3 = util.format(queryString_recent, userId);
var result6 = yield databaseUtils.executeQuery(query3);
console.log(result6[0]);

yield this.render('vendordetail',{
userDetails4 : result4[0],
userDetails5 : result5,
userDetails6 : result6
});
    },
    productPage: function*(next){
    var queryproduct = 'select product.p_id,name,image_url,available from product left join review on product.p_id=review.p_id group by name;'
    var result = yield databaseUtils.executeQuery(queryproduct);   
    yield this.render('product',{
        product: result
    })
    },
    productdetailPage: function*(next){

        userId= this.params.id;
        var queryString_Details = 'select * from product where p_id = "%s"';
        var query1 = util.format(queryString_Details, userId);
        var result4 = yield databaseUtils.executeQuery(query1);
        var queryString_avg_rating = 'SELECT AVG(rating) as avg FROM review WHERE p_id="%s"';
        var query2 = util.format(queryString_avg_rating, userId);
        var result5 = yield databaseUtils.executeQuery(query2);
         var queryString_review = 'select review.id, name ,rating,review,feedback,count(feedback) AS count,review.creation_timestamp from (review inner join user on user.u_id=review.u_id ) left join feedback on review.id=feedback.id where p_id="%s" and verified = 0 GROUP BY id,feedback ORDER BY id,feedback';
        var query3 = util.format(queryString_review, userId);
        var result6 = yield databaseUtils.executeQuery(query3);
        var fr = {};
        for(var ele in result6){
        console.log(result6[ele]);
        var id = result6[ele]['id'];
        var name = result6[ele]['name'];
        var rating = result6[ele]['rating'];
        var review = result6[ele]['review'];
        var feedback = result6[ele]['feedback'];
        var count = result6[ele]['count'];
        var creation_timestamp = result6[ele]['creation_timestamp'];
        if(fr[id]==undefined){
        fr[id]={};
        fr[id]['name']=name;
        fr[id]['rating']=rating;
        fr[id]['review']=review;
        fr[id]['creation_timestamp']=creation_timestamp;
        console.log(fr[id]);
        fr[id][feedback]=count;
        console.log(fr);
        }
        else{
        fr[id][feedback]=count;
        }

        }
        console.log(fr);
        user_reviews=[];
        for(var i in fr){
        var tmp={};
        console.log(i);
        tmp['id']=i;
        tmp['details']=fr[i];
        user_reviews.push(tmp);
        }
        console.log(user_reviews);
        //console.log(result5);
        var queryString_vendor = 'SELECT * FROM vendor where u_id in (Select distinct(u_id) from Product JOIN vendor_product ON product.p_id=vendor_product.p_id where product.p_id="%s")';
        var query4 = util.format(queryString_vendor, userId);
        var result7 = yield databaseUtils.executeQuery(query4);

        var r5 = 'select count(*) as r5 from review join product on review.p_id = product.p_id where rating=5 and product.p_id="%s";';
        var r4 = 'select count(*) as r4 from review join product on review.p_id = product.p_id where rating=4 and product.p_id="%s";';
        var r3 = 'select count(*) as r3 from review join product on review.p_id = product.p_id where rating=3 and product.p_id="%s";';
        var r2 = 'select count(*) as r2 from review join product on review.p_id = product.p_id where rating=2 and product.p_id="%s";';
        var r1 = 'select count(*) as r1 from review join product on review.p_id = product.p_id where rating=1 and product.p_id="%s";';
        var queryr5 = util.format(r5,userId);
        var res5 = yield databaseUtils.executeQuery(queryr5);
            console.log(queryr5);
                console.log(res5);
        var queryr4 = util.format(r4,userId);
        var res4 = yield databaseUtils.executeQuery(queryr4);

        var queryr3 = util.format(r3,userId);
        var res3 = yield databaseUtils.executeQuery(queryr3);

        var queryr2 = util.format(r2,userId);
        var res2 = yield databaseUtils.executeQuery(queryr2);

        var queryr1 = util.format(r1,userId);
        var res1 = yield databaseUtils.executeQuery(queryr1);
                console.log(res5[0].r5);
            var avg= ((res5[0].r5)*25 + (res4[0].r4)*16 +   (res3[0].r3)*9    +   (res2[0].r2)*4    +   (res1[0].r1)*1 )/((res5[0].r5)*5 + (res4[0].r4)*4 +   (res3[0].r3)*3    +   (res2[0].r2)*2    +   (res1[0].r1)*1);

            console.log(avg);

        yield this.render('productdetail',{
        userDetails4 : result4[0],
        userDetails5 : result5,
        userDetails6 : result6,
        userDetails7 : result7,
        user_reviews : user_reviews,
        avg_rating: avg

        });
   },
        addproductPage: function* (next){
            userID = this.params.slug;
            
            yield this.render('addproduct',{
                 user: userID
            });
        },   
            actionpage: function* (next){
                console.log("---------------------------------------------arpit-----------------------------------------------");
                var nam = this.request.body.pname;
                var desc = this.request.body.desc;
                var img = this.request.body.img;
                var faq = this.request.body.faq;
                var uid = this.request.body.u_id;
                console.log(nam);
                console.log(uid);
                var query = 'insert into product (name,description,image_url,faq,product_url) values ("%s","%s","%s","%s","NULL");';
                var query4 = util.format(query,nam,desc,img,faq);
                var result = yield databaseUtils.executeQuery(query4);


                 query = 'insert into vendor_product (p_id,u_id,creation_timestamp) values ("%s","%s",now());';
                 query4 = util.format(query,result.insertId,uid);
                var result22 = yield databaseUtils.executeQuery(query4);

                    console.log("----------------------------------------------------------------------------------------------"+result22);

               query="https://localhost:3000/app/"+result.insertId;
               console.log("----------------------------------------------------------------------"+query);
                    query4 = 'update product set product_url = "%s" where p_id="%s";';
                    var res = util.format(query4,query,result.insertId);
                   result =  yield databaseUtils.executeQuery(res);

                    console.log("-----------------------------------------------------------------------------------------"+result.insertId);
                     yield this.render('add',{
                    
               });
            },
    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
        
        this.redirect('/');
    }
}
