var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    showAllProducts: function*(next){
    var queryproduct = 'select product.p_id,name,image_url,available,rating from product left join review on product.p_id=review.p_id group by name ;'
    

    
    
    var products_list = yield databaseUtils.executeQuery(queryproduct);
    for(var i in products_list)
    {
        var productId = products_list[i].p_id;
        queryproduct = 'select (select count(*) from review where p_id = "%s" and rating = 5)as r5,(select count(*) from review where p_id = "%s" and rating = 4)AS r4,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 3)as r3,(select count(*) from review where p_id = "%s" and rating = 2)AS r2,(select count(*) from review where p_id = "%s" and rating = 1) as r1';
        
        var query = util.format(queryproduct,productId,productId,productId,productId,productId);
        var result= yield databaseUtils.executeQuery(query);
             var avg= ((result[0].r5)*25 + (result[0].r4)*16 +   (result[0].r3)*9    +   (result[0].r2)*4    +   (result[0].r1)*1 )/((result[0].r5)*5 + (result[0].r4)*4 +   (result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1);
                
             products_list[i].rating=avg;
    }
    yield this.render('products',{
        products_list: products_list
    })
    },
    showProductDetailPage: function*(next){
        var added=0;
        var productId= this.params.id;
        var queryString = 'select * from product where p_id = "%s" and available=1';
        var query = util.format(queryString, productId);
        var result = yield databaseUtils.executeQuery(query);
        var product_details = result[0];
        queryString = 'select review.id,review.u_id, name ,rating,review,feedback,count(feedback) AS count,review.creation_timestamp from (review inner join user on user.u_id=review.u_id ) left join feedback on review.id=feedback.id where p_id="%s" and verified = 0 GROUP BY id,feedback ORDER BY id,feedback';
        query= util.format(queryString, productId);
        result = yield databaseUtils.executeQuery(query);
        var user_reviews=result;
        var fr = {};
        for(var ele in user_reviews){
        var id = user_reviews[ele]['id'];
        var uid = user_reviews[ele]['u_id'];
        var name = user_reviews[ele]['name'];
        var rating = user_reviews[ele]['rating'];
        var review = user_reviews[ele]['review'];
        var feedback = user_reviews[ele]['feedback'];
        var count = user_reviews[ele]['count'];
        var creation_timestamp = user_reviews[ele]['creation_timestamp'];
        if(fr[id]==undefined){
        fr[id]={};
        fr[id]['uid']=uid;
        fr[id]['name']=name;
        fr[id]['rating']=rating;
        fr[id]['review']=review;
        fr[id]['creation_timestamp']=creation_timestamp;
        fr[id][feedback]=count;
        }
        else{
          fr[id][feedback]=count;
        }
        }
        user_reviews_array=[];
        for(var i in fr){
        var tmp={};
        console.log(i);
        tmp['id']=i;
        tmp['details']=fr[i];
        user_reviews_array.push(tmp);
        }

        if(this.currentUser){
            var cid= this.currentUser.u_id;
            console.log(cid);
            for( var i in user_reviews_array ){
            if(user_reviews_array[i].details.uid == cid){
            added = 1;
            break;
            }
            }
            console.log(added);
            }

        queryString = 'SELECT * FROM vendor where u_id in (Select distinct(u_id) from Product JOIN vendor_product ON product.p_id=vendor_product.p_id where product.p_id="%s")';
        query= util.format(queryString, productId);
        result = yield databaseUtils.executeQuery(query);
        var vendors_list= result;



        querystring = 'select (select count(*) from review where p_id = "%s" and rating = 5)as r5,(select count(*) from review where p_id = "%s" and rating = 4)AS r4,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 3)as r3,(select count(*) from review where p_id = "%s" and rating = 2)AS r2,(select count(*) from review where p_id = "%s" and rating = 1) as r1';
        query = util.format(querystring,productId,productId,productId,productId,productId);
        result= yield databaseUtils.executeQuery(query);
        var avg= ((result[0].r5)*25 + (result[0].r4)*16 +   (result[0].r3)*9    +   (result[0].r2)*4    +   (result[0].r1)*1 )/((result[0].r5)*5 + (result[0].r4)*4 +   (result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1);


        yield this.render('productdetail',{
        product_details : product_details,
        
        user_reviews : user_reviews,
        vendors_list : vendors_list,
        user_reviews_array : user_reviews_array,
        avg_rating: avg,
        added:added

        });
   },
        addproductPage: function* (next){
            userID = this.params.slug;

            yield this.render('addproduct',{
                 user: userID
            });
        },
            showaddproductPage: function* (next){
                console.log("1");
                var uploadedFiles = this.request.body.files;
                var nam = this.request.body.fields.pname;
                var desc = this.request.body.fields.desc;
                var uid = this.request.body.fields.u_id;

                this.body = uploadedFiles;
                var url=this.body.img.path;
                url=url.replace("static\\static","\\static")
                var iurl= url.replace(/\\/g,'\\\\');
                url=this.body.faq.path;
                url=url.replace("static\\static","\\static")
                var furl= url.replace(/\\/g,'\\\\');
               
                
                
                        var query='select p_id from product where name="%s";';
                        query = util.format(query,nam);
                        var result=yield databaseUtils.executeQuery(query);
                        var id;
                        if(result.length>0)
                        {
                            console.log("2");
                           id=result[0].p_id;  
                        }
                        else{
                            console.log("3");
                            query = 'insert into product (name,description,image_url,faq,product_url) values ("%s","%s","%s","%s","//");';
                            query = util.format(query,nam,desc,iurl,furl);
                           result = yield databaseUtils.executeQuery(query);
                         var  purl='app//product//'+result.insertId;
                           id= result.insertId;
                           query= 'update product set product_url = "%s" where p_id="%s";';
                           query = util.format(query,purl,result.insertId);
                          result =  yield databaseUtils.executeQuery(query);
                        }

                        console.log("4");
                //result = yield databaseUtils.executeQuery('update product set product_url='+purl);
                query = 'update product set available=1 where p_id="%s";';
                 query = util.format(query,id);
                yield databaseUtils.executeQuery(query);
                 query = 'insert into vendor_product (p_id,u_id,creation_timestamp) values ("%s","%s",now());';
                 query = util.format(query,id,uid);
                var result22 = yield databaseUtils.executeQuery(query);

                     this.redirect('/app/vendor/'+uid);
               
            },
            showAddReviewPage: function* (next){
                var productid = this.params.id;
                console.log(productid);
                yield this.render('addreview',{
                productid: productid
                });
                },

                showReviewPage: function* (next){
                var uid = this.currentUser.u_id;
                var pid= this.params.id;
                var rating = this.request.body.rating;
                var review = this.request.body.review;
                console.log(uid);
                console.log(pid);
                console.log(rating);
                console.log(review);
                var queryString = 'insert into review(p_id,u_id,rating,review,creation_timestamp) values("%s","%s","%s","%s",now())';
                var query= util.format(queryString, pid,uid,rating,review);
                var errorMessage;
                try{
                var result= yield databaseUtils.executeQuery(query);
                queryString = 'select * from review where id=%s';
                query= util.format(queryString, result.insertId);
                result= yield databaseUtils.executeQuery(query);
                var insertedreview = result[0];
                }
                catch(e){
                if(e.code === 'ER_DUP_ENTRY'){
                errorMessage= 'Review already exists';
                }else{
                throw e;
                }
                }
                if(errorMessage){
                console.log(errorMessage)
                yield this.render('home',{
                errorMessage:errorMessage
                })
                }else {
                console.log('review added');
                this.redirect('/app/product/'+pid);
                }
                },


                disableProduct:function* (next){

                    var uid=this.request.body.u_id;
                    var pid=this.request.body.p_id;
                    console.log(uid+'---------------------------------'+pid);
                 try{
                    var queryString = 'update vendor_product set available = 0 where p_id="%s" and u_id="%s";';
                    var query = util.format(queryString,pid,uid);
                    var result = yield databaseUtils.executeQuery(query);
                    console.log(result +'    '+ query);

                    queryString='select count(*) from vendor_product  where p_id="%s" and available=1; ';
                    query = util.format(queryString,pid);
                    result = yield databaseUtils.executeQuery(query);
                    console.log(result +'    '+ query);

                
                if(result.length!=0)
                {
                    queryString='update product set available=0 where p_id="%s";';
                    query = util.format(queryString,pid);
                    result = yield databaseUtils.executeQuery(query);  
                    console.log(result +'    '+ query);
   
                }

                
                
                }
                catch(e){

                    throw new Error(e);
                }
                this.body='product disabled';

                },
                
    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    }
}
