var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
     showCustomerDetailPage: function* (next) {
        var userId = this.params.id;
        var queryString ='select * from user where u_id ="%s"';
        var query=util.format(queryString,userId);
        var result=yield databaseUtils.executeQuery(query);
        var user_details = result[0];
        queryString='SELECT p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" ,r.creation_timestamp FROM product AS p JOIN review AS r ON p.p_id = r.p_id WHERE r.p_id IN (SELECT r.p_id FROM review GROUP BY r.u_id HAVING r.u_id = "%s" ORDER BY creation_timestamp) GROUP BY p.p_id ORDER BY AVG(r.rating)';
        query=util.format(queryString,userId);
        result=yield databaseUtils.executeQuery(query);
        var reviewed_products_list = result;
        yield this.render('customerdetailpage',{
            reviewed_products_list:reviewed_products_list,
            user_details:user_details,
           
        });
    },
    showAllCustomers: function* (next){
                var query = 'select * from user join user_role on user.u_id=user_role.u_id where user_role.r_id=1;';
                var result = yield databaseUtils.executeQuery(query);
                yield this.render('customers',{
                    customers_list: result
                })
    },
    showVendorsPage: function* (next){
        var query = 'select * from vendor;';
        var vendors_list = yield databaseUtils.executeQuery(query);
        yield this.render('vendors',{
            vendors_list: vendors_list
        });
    },

    showchangepasswordpage: function* (next){
        yield this.render('changepasswordPage',{
           
        });
    },
    changepassword: function* (next){

        var oldpassword= this.request.body.password;
        var mob=this.request.body.mob;
        var password = this.request.body.psw;


        var queryString = 'select * from user where mob="%s"';
        var query= util.format(queryString, mob);
        var result = yield databaseUtils.executeQuery(query);
        
        var errorMessage;
        if(result.length == 0)
        {
        errorMessage = "ERROR";
        throw new Error("Mobile number not found");
        } 
        else{
        var user=result[0];
        if(user.password ===oldpassword)
        {
        //sessionUtils.saveUserInSession(user,this.cookies);
        queryString='update user set password ="%s" where mob="%s" ;';
        query= util.format(queryString, password,mob);
        result = yield databaseUtils.executeQuery(query);
errorMessage='password reset';
        }
        }
    this.body=errorMessage;
        
        },


        showeditprofilepage: function* (next){
            yield this.render('editprofilePage',{
               
            });
        },
        editprofile:function* (next){
           var uploadedFiles = this.request.body.files;
            var mob=this.request.body.fields.mob;
            var name=this.request.body.fields.name;
            var gender=this.request.body.fields.gender;
            var user_email=this.request.body.fields.user_email;
            var dob =this.request.body.fields.dob;
            this.body = uploadedFiles;
            var url=this.body.img.path;
            url=url.replace("static\\static","\\static")
            var furl= url.replace(/\\/g,'\\\\');
 
            var queryString = 'select * from user where mob="%s"';
            var query= util.format(queryString, mob);
            var result = yield databaseUtils.executeQuery(query);
            var u_id=result[0].u_id;
            console.log(query);
            var errorMessage;
            if(result.length == 0 ){
            errorMessage = "ERROR";
            yield this.render('editprofilePage',{
            errorMessage:errorMessage
            })
            } 
            else{
               
                if(name){
                  
                    queryString='update user set name ="%s" where mob="%s" ;';
                    query= util.format(queryString,name,mob);
                    result = yield databaseUtils.executeQuery(query);
                   
                }
                if(user_email)
                {
                    console.log("-------------------------------email-------------------------");
                    queryString='update user set user_email ="%s" where mob="%s" ;';
                    query= util.format(queryString,user_email,mob);
                    result = yield databaseUtils.executeQuery(query);
                }
                if(dob)
                {
                    console.log("-------------------------------dob--------------------------");
                    queryString='update user set dob ="%s" where mob="%s" ;';
                    query= util.format(queryString,dob,mob);
                    result = yield databaseUtils.executeQuery(query);
                }
                if(gender)
                {
                    console.log("-------------------------------gender--------------------------");
                    queryString='update user set gender ="%s" where mob="%s" ;';
                    query= util.format(queryString,gender,mob);
                    result = yield databaseUtils.executeQuery(query);
                }
                if(furl)
                {
                    console.log("-------------------------------img--------------------------");
                    queryString='update user set img ="%s" where mob="%s" ;';
                    query= util.format(queryString,furl,mob);
                    result = yield databaseUtils.executeQuery(query);
                
                }
              
var crt_user='select * from user join user_role on user.u_id=user_role.u_id where mob="%s"';
var c_query=util.format(crt_user,mob);

var rst=yield databaseUtils.executeQuery(c_query);
console.log(rst);
sessionUtils.updateUserInSession(rst[0],this.cookies);

                
                this.redirect('/app/customer/'+u_id);                                                                                               








        }
            },



        showloginpage: function* (next){
            var errorMessage='';
            yield this.render('loginPage',{
    
                errorMessage:errorMessage
            });
        },
        login: function* (next){

            var username= this.request.body.mob;
            console.log(username);
            var password = this.request.body.psw;
            var queryString = 'select * from user join user_role on user.u_id= user_role.u_id where mob="%s"';
            var query= util.format(queryString, username);
            var result = yield databaseUtils.executeQuery(query);
            
            var errorMessage = 'loged in';
            console.log(result[0]);
            console.log(errorMessage+result.length);
            if(result.length == 0)
            {
                errorMessage = "ERROR";
                console.log(errorMessage);
                    this.body=errorMessage;
            } else{
                var user=result[0];
                if(user.password === password)
                {
                    sessionUtils.saveUserInSession(user,this.cookies);
                    
                }else{
                errorMessage = "WRONG PASSWORD";
               
                
                    }
            }
            this.body=errorMessage;
         /*   if(errorMessage)
            {
                 this.body=errorMessage;
            }
            else {
                        
        }*/
            
            },

        logout: function* (next) {
            var sessionId = this.cookies.get("SESSION_ID");
            if(sessionId) {
                sessionUtils.deleteSession(sessionId);
            }
            this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
    
            this.redirect('/app/home');
        },


        signup:  function* (next) {
            console.log('inside sign up');
            var uploadedFile = this.request.body.files;
            console.log(uploadedFile);
           var logo_url=uploadedFile.logo.path;
           var gst_url=uploadedFile.gst_certi.path;
            var errorMessage="" ;
            var result;
          try{ 
         
            
            
            
             
              console.log(logo_url);
                logo_url=logo_url.replace("static\\static","\\static")
                logo_url = logo_url.replace(/\\/g,'\\\\');
                var firmname=this.request.body.fields.fname;
                var location=this.request.body.fields.location;
                var gst_no=this.request.body.fields.gst_no;
                var pan=this.request.body.fields.pan;
               
                gst_url=gst_url.replace("static\\static","\\static")
                    gst_url = gst_url.replace(/\\/g,'\\\\');
                
               
               
               
                var user_details = 'update user_role set r_id=2 where u_id="%s";';
                 var   query = util.format(user_details,this.currentUser.u_id);
                yield databaseUtils.executeQuery(query);
               
               
               
            
                 var vendor_details = 'insert into vendor(firm_name,location,logo,gst_no,pan_no,gst_certi,u_id) values("%s","%s","%s","%s","%s","%s","%s");';
                 query = util.format(vendor_details,firmname,location,logo_url,gst_no,pan,gst_url,this.currentUser.u_id);
             result = yield databaseUtils.executeQuery(query); 
             this.redirect('/app/home');
           
        }
        catch(e){
            if(e.code === 'ER_DUP_ENTRY'){
            errorMessage= 'User already exists';
            }
            else
            {
            throw e;
            }
            }
            if(errorMessage){
            console.log(errorMessage);
            yield this.render('signupPage',{
            errorMessage:errorMessage
            });
        }


        var crt_user='select * from user join user_role on user.u_id=user_role.u_id where user.u_id="%s"';
var c_query=util.format(crt_user,this.currentUser.u_id);

var rst=yield databaseUtils.executeQuery(c_query);
console.log(rst);
sessionUtils.updateUserInSession(rst[0],this.cookies);
   },




   signupascustomer:  function* (next) {
            
            console.log('sign up as customer');
            var name=this.request.body.name;
            var mob_no=this.request.body.mob;
            var password=this.request.body.psw;
            var email=this.request.body.email;
           var user_details = 'insert into user(name,mob,password,user_email,creation_timestamp) values("%s","%s","%s","%s",now());';
           var errorMessage = '';
          var query = util.format(user_details,name,mob_no,password,email);
          try{ 
          var result = yield databaseUtils.executeQuery(query);
            var user_details = 'insert into user_role(u_id,r_id) values("%s",1);';
            var quer = util.format(user_details,result.insertId);
             resul = yield databaseUtils.executeQuery(quer);
              errorMessage="signed up sucessfully" ;
             this.body = errorMessage;
            }
        catch(e){
            if(e.code === 'ER_DUP_ENTRY'){
            errorMessage= 'User already exists';
            throw new Error("User information already exit");
            }
            else
            {
            throw e;
            }
            }
            if(errorMessage){
            this.body=errorMessage;
           
        }
   },
        
            
         showsignupasVendorPage: function* (next) {
            yield this.render('signupasVendorPage',{
            });
            },

            showAdminPage: function* (next) {
                yield this.render('adminPage',{
                });
                },
    
                
            
    showVendorDetailPage: function* (next){
        userId= this.params.u_id;
        var queryString = 'select * from vendor join user on vendor.u_id=user.u_id where vendor.u_id="%s";';
        var query= util.format(queryString, userId);
        var result = yield databaseUtils.executeQuery(query);
        var vendor_details=result[0];
        queryString = 'select name,image_url,vendor_product.p_id,avg(rating) as avg,product.available, vendor_product.creation_timestamp from ((product join vendor_product on vendor_product.p_id=product.p_id) left join review on review.p_id=vendor_product.p_id) where vendor_product.u_id="%s" and vendor_product.available=1 group by product.p_id;'
        query = util.format(queryString, userId);
        result = yield databaseUtils.executeQuery(query);
        var vendor_products_list = result;
        queryString= 'SELECT name,image_url,creation_timestamp FROM product JOIN vendor_product ON product.p_id=vendor_product.p_id where u_id="%s" and vendor_product.available=1 order by creation_timestamp desc limit 5;';
        query= util.format(queryString, userId);
        result = yield databaseUtils.executeQuery(query);
        var recent_products=result;
        yield this.render('vendordetail',{
          vendor_details : vendor_details,
          vendor_products_list : vendor_products_list,
          recent_products : recent_products
        });
    }
}
