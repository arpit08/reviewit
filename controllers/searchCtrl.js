var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
     showSearchPage: function* (next) {
       var name = this.request.query.name;
       var search = this.request.query.search;
       var queryString,query,result,isVendor=false;
       if(search ==="Product")
       {
       queryString = 'select product.p_id,name,image_url,avg(rating) as rating from product left join review on product.p_id=review.p_id where name like "%s" and product.available=1 group by name;';
       query=util.format(queryString,"%"+name+"%");
       }
       else{
           queryString = 'select * from vendor where firm_name like "%s"';
           query=util.format(queryString,"%"+name+"%");
           isVendor=true;
      }
       result= yield databaseUtils.executeQuery(query);
       var details=result;
       yield this.render('search',{
                // TODO rename product to details
               product: details,
               isvendor: isVendor
       });
    },
}


