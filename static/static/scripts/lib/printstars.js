
document.getElementById("stars").innerHTML =getavgStars(document.getElementById("stars").getAttribute('data-val'));

var x= document.getElementsByClassName("reviewstars");

for(var ele in x) {
var a = x[ele].innerText;
x[ele].innerHTML = getrevStars(a) ;
}
function getavgStars(rating) {
// Round to nearest half
rating = Math.round(rating * 2) / 2;
let output = [];
// Append all the filled whole stars
for (var i = rating; i >= 1; i--)
output.push('<i class="fa fa-star fa-5x" aria-hidden="true" style="color: gold; "></i>&nbsp;');
// If there is a half a star, append it
if (i == .5) output.push('<i class="fa fa-star-half-o fa-5x" aria-hidden="true" style="color: gold;"></i>&nbsp;');
// Fill the empty stars
for (let i = (5 - rating); i >= 1; i--)
output.push('<i class="fa fa-star-o fa-5x" aria-hidden="true" style="color: gold;"></i>&nbsp;');

return output.join('');

}

function getrevStars(rating) {
// Round to nearest half
rating = Math.round(rating * 2) / 2;
let output = [];
// Append all the filled whole stars
for (var i = rating; i >= 1; i--)
output.push('<i class="fa fa-star fa-3x" aria-hidden="true" style="color: gold;"></i>&nbsp;');
// If there is a half a star, append it
if (i == .5) output.push('<i class="fa fa-star-half-o fa-3x" aria-hidden="true" style="color: gold;"></i>&nbsp;');
// Fill the empty stars
for (let i = (5 - rating); i >= 1; i--)
output.push('<i class="fa fa-star-o fa-3x" aria-hidden="true" style="color: gold;"></i>&nbsp;');
return output.join('');
}

var votes= document.getElementsByClassName("votes");


/*
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
if (event.target == modal) {
modal.style.display = "none";
}
}*/